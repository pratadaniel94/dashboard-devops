from flask import Blueprint, render_template
import requests

gitlab = Blueprint('gitlab', __name__, url_prefix="/gitlab")
token = {'private_token': 'z-47TPzCVQ5CvEDK2BMD'}
uri = 'http://127.0.0.1:9090/api/v4/'

@gitlab.route('')
def index():
    projetos = requests.get(uri+'projects',token)
    users = requests.get(uri+'users',token)
    return render_template('gitlab.html', projetos=projetos.json(), users=users.json())